public class Circle extends Ellipse {

    public Circle(int x, int y, int radius1) {
        super(x, y, radius1, radius1);
    }

    public int getPerimeter() {
        int a = (int) (2 * Math.PI * radius1);
        System.out.println("Длина окружности = " + a);
        return a;
    }
}
