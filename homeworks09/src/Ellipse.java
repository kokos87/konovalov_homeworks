public class Ellipse extends Figure {
    int radius1;
    int radius2;

    public Ellipse(int x, int y, int radius1, int radius2) {
        super(x, y);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public int getPerimeter() {
        int a = (int) (Math.PI * (radius1 + radius2));
        System.out.println("Длина эллипса = " + a);
        return a;
    }
}
