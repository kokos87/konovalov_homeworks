public class Rectangle extends Figure {
    int length;
    int width;

    public Rectangle(int x, int y, int length, int width) {
        super(x, y);
        this.length = length;
        this.width = width;
    }

    public int getPerimeter() {
        int a = length * 2 + width * 2;
        System.out.println("Периметр прямоугольника = " + a);
        return a;
    }
}
