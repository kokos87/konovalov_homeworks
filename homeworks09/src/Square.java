public class Square extends Rectangle {
    public Square(int x, int y, int length) {
        super(x, y, length, length);
    }

    public int getPerimeter() {
        int a = length * 4;
        System.out.println("Периметр квадрата = " + a);
        return a;
    }
}
