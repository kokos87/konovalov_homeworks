create table product
(
    id          serial primary key,
    description varchar(20),
    cost        integer,
    quantity    integer
);

create table customers
(
    id         serial primary key,
    first_name varchar(20),
    last_name  varchar(20)
);

create table orders
(
    id_product  integer,
    id_customer integer,
    date        date,
    foreign key (id_product) references product (id),
    foreign key (id_customer) references customers (id)
);

insert into product (description, cost, quantity) values ('Milk', 80, 150);
insert into product (description, cost, quantity) values ('Bread', 50, 250);
insert into product (description, cost, quantity) values ('Cheese', 230, 70);
insert into product (description, cost, quantity) values ('Tea', 85, 30);
insert into product (description, cost, quantity) values ('Oil', 130, 35);

insert into customers(first_name, last_name) values ('Petrov', 'Ivan');
insert into customers(first_name, last_name) values ('Andreev', 'Roman');
insert into customers(first_name, last_name) values ('Bondarevsky', 'Nikolay');
insert into customers(first_name, last_name) values ('Pirozhkov', 'Artur');
insert into customers(first_name, last_name) values ('Mikhailov', 'Pavel');

insert into orders (id_product, id_customer, date) values (1, 2, '01.01.2021');
insert into orders (id_product, id_customer, date) values (2, 1, '02.01.2021');
insert into orders (id_product, id_customer, date) values (3, 3, '03.01.2021');
insert into orders (id_product, id_customer, date) values (4, 2, '04.01.2021');
insert into orders (id_product, id_customer, date) values (5, 1, '05.01.2021');
insert into orders (id_product, id_customer, date) values (2, 4, '06.01.2021');

-- вывод продуктов с сортировкой цены по возрастанию
select description, cost
from product
order by cost desc;

-- вывод проданных товаров и тех, кто и когда их купил
select (select description as товар from product where id = orders.id_product),
       (select first_name as покупатель from customers where id = orders.id_customer),
    date as дата_покупки
from orders;

-- вывод числа продаж по каждой позиции товара
select (select description as Товар from product where id = orders.id_product),
       count(*) as Число_продаж
from orders
group by id_product;

-- вывод всех данных из таблиц customer и product
select *
from orders a
         full join customers c on c.id = a.id_customer;
