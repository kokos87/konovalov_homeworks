/*
На вход подается строка с текстом. Необходимо посчитать, сколько встречается раз каждое слово в этой строке.

Вывести:
Слово - количество раз

Использовать Map, string.split(" ") - для деления текста по словам. Слово - символы, ограниченные пробелами справа и слева.

 */

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        String inputString = "слово1 слово2 слово1 слово3 слово1 слово4 слово2";
        Map<String, Integer> mapStrings = new HashMap<>();
        String[] strArray = inputString.split(" "); // разделяем входную строку на слова по пробелам
        for (String s : strArray) {
            if (mapStrings.containsKey(s)) { // если в мапе уже встречается такое слово,
                                             // инкрементируем соответствующее ему значение
                mapStrings.put(s, mapStrings.get(s) + 1);
            } else {    // в противном случае добавлем его в мапу и индексируем единицей
                mapStrings.put(s, 1);
            }
        }
        for (Map.Entry<String, Integer> entry : mapStrings.entrySet()) { // выводим мапу на экран
            System.out.println(entry.getKey() + " - " + entry.getValue() + ";");
        }
    }
}
